'use client'
import { GET_ALL_POKEMON } from '@/gqls/getAllPokemon.gql';
import { IPokemonAPI } from '@/gqls/pokemon.types';
import { useSuspenseQuery } from "@apollo/experimental-nextjs-app-support/ssr";

const usePokemonData = () => {
  
  const {  data:pokemonData, error: queryError } = useSuspenseQuery<IPokemonAPI>(GET_ALL_POKEMON);
  return { pokemonData, queryError };
};

export default usePokemonData;