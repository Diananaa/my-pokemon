import Link from "next/link"
import { IHeadeProps } from "./header.type"

const Header = (props: IHeadeProps) => {
    return (
        <div className="flex justify-start items-center w-full">
            <Link href={'/'}>
                <p className="hover:bg-slate-300 bg-slate-50 px-3 py-3 rounded-lg hover:cursor-pointer border ">Back</p>
            </Link>
            <p className="w-full flex text-2xl font-bold pl-5">{props.title}</p>
        </div>
    )
}
export default Header