const PokemonListSkeleton = () => {
    return (
        <div className="space-x-1 bg-slate-200 md:space-y-4 w-full flex justify-center items-center flex-col border p-5 rounded-lg md:rounded-2xl">
            <div className="animate-pulse  h-full flex items-center flex-col w-full">
                <div className='h-8 bg-slate-700 space-x-1 md:space-y-4 w-full flex justify-center items-center flex-col border  rounded-lg md:rounded-2xl'></div>
                <div className='bg-slate-700 my-[80px] h-[100px] w-[100px] rounded-full'></div>
                <div className='h-8 bg-slate-700  md:space-y-4 w-[200px] flex justify-center items-center flex-col border  rounded-lg md:rounded-2xl'></div>
            </div>
        </div>
    )
}

export default PokemonListSkeleton