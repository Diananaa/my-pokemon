import { typePokemon } from "@/gqls/pokemon.types"

export interface IPropsListCard {
    color: string,
    height: number,
    backSprite: string,
    name: string,
    species: string,
    types: typePokemon[]
    weight: number
}