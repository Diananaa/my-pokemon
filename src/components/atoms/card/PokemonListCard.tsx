import { DATA_DETAIL_POKEMON } from "@/context/pokemonContext"
import { useAtom } from "jotai"
import Image from "next/image"
import Link from "next/link"
import { IPropsListCard } from "./PokemonListCard.type"

const PokemonListCard = (props: IPropsListCard) => {
    const [, SET_DETAIL_DATA] = useAtom(DATA_DETAIL_POKEMON)
    const onClick = () => SET_DETAIL_DATA(
        {
            color: props.color,
            height: props.height,
            backSprite: props.backSprite,
            key: props.name,
            species: props.species,
            types: props.types,
            weight: props.weight
        }
    )
    return (
        <Link href={`/${props.name}`} onClick={onClick}>
            <div className='bg-slate-200 hover:cursor-pointer hover:bg-slate-300 space-x-1 md:space-y-4 w-full flex justify-center items-center flex-col border p-5 rounded-lg md:rounded-2xl'>
                <p className='text-2xl font-bold uppercase'>{props.name}</p>
                <Image alt={props?.name} src={props?.backSprite} width={200} height={200} />
                <div className='space-x-3 flex text-lg'>
                    {props?.types?.map((type, id) => <p key={id}>{type?.name}</p>)}
                </div>
            </div >
        </Link>
    )
}
export default PokemonListCard