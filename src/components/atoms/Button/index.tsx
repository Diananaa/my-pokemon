import { IButtonProps } from "./Button.type"

const Button = (props: IButtonProps) => {
    return (
        <button className="hover:cursor-pointer px-2 py-5 rounded-xl font-bold hover:bg-blue-300 bg-blue-100" onClick={props.onClick}>
            {props.title}
        </button>
    )
}
export default Button