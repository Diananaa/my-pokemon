'use client'

import { Button } from "@/components/atoms"
import { useState } from "react"

const NavbarSearch = () => {
    const [search, setSearch] = useState<string>('')
    const onClick = () => {
        console.log('hello', search)
    }
    return (
        <div className="flex w-full justify-between">
            <p>POKEMON</p>
            <div className="flex">
                <input
                    type="text"
                    value={search}
                    placeholder="search pokemon"
                    onChange={(e) => setSearch(e.target.value)}
                    className="border rounded-xl py-3 px-6"
                />
                <div className="">
                    <Button title="Search" onClick={onClick} />
                </div>
            </div>
        </div>
    )
}
export default NavbarSearch