'use client'
import { PokemonListCard } from '@/components/atoms';
import { PokemonListSkeleton } from '@/components/organism';
import { IPokemonAPI } from '@/gqls/pokemon.types';
import usePokemonData from '@/hooks/usePokemonData';
import { useEffect, useState } from 'react';

export default function Home() {
  const { pokemonData, queryError } = usePokemonData();
  const [getPokemon, setGetPokemon] = useState<IPokemonAPI>();
  useEffect(() => {
    setGetPokemon(pokemonData);
  }, [pokemonData]);

  const DATA_ALL_TYPE = [...new Set(getPokemon?.getAllPokemon?.flatMap(item => item.types.map(type => type.name)))];

  const filteredData = (category: string[]) => {
    const filter = getPokemon?.getAllPokemon.filter(item =>
      category.some(targetType =>
        item.types.some(type => type.name.toLowerCase() === targetType.toLowerCase())
      )
    );
    const getAllPokemonData:IPokemonAPI  = {
      getAllPokemon: filter
    }
    setGetPokemon(getAllPokemonData)
  };

  return (
    <main className=''>
      <div className='flex  min-w-[100px] flex-wrap px-5 gap-3 mb-5 lg:max-w-5xl'>
        <div
          onClick={() => {
            setGetPokemon(pokemonData)
          }}
          className='bg-white hover:cursor-pointer text-black text-lg px-5 py-2 rounded-l-2xl rounded-r-2xl hover:bg-slate-200 border border-blue-950'
        >
          ALL CATEGORY
        </div>
        {
          DATA_ALL_TYPE?.map((item, key) => (
            <div
              key={key}
              onClick={() => {
                filteredData([item]);
              }}
              className='bg-white hover:cursor-pointer text-black text-lg px-5 py-2 rounded-l-2xl rounded-r-2xl hover:bg-slate-200 border border-blue-950'
            >
              {item}
            </div>
          ))
        }
      </div>
      <div className='grid w-[100vw] lg:max-w-5xl justify-items-center px-5 gap-4 sm:grid-cols-2 lg:grid-cols-3 '>
        {
          queryError ? <p>Oh no, there was an error</p>
            : !getPokemon?.getAllPokemon ? (
              <>
                {[...Array(9)].map((_, index) => (
                  <PokemonListSkeleton key={index} />
                ))}
              </>
            )
              : getPokemon?.getAllPokemon ?
                getPokemon.getAllPokemon.map((item, id) => (
                  <PokemonListCard
                    key={id}
                    color={item.color}
                    height={item.height}
                    backSprite={item.backSprite}
                    name={item.key}
                    species={item.species}
                    types={item.types}
                    weight={item.weight}
                  />
                ))
                : null
        }
      </div>
    </main>
  )
}
