'use client'

import Header from "@/components/organism/Header"
import { DATA_DETAIL_POKEMON } from "@/context/pokemonContext"
import { useAtom } from "jotai"
import Image from "next/image"
import { redirect } from "next/navigation"
import { useEffect } from "react"

const page = () => {
    const [DETAIL_DATA,] = useAtom(DATA_DETAIL_POKEMON)
    const { color, height, key, backSprite, species, types, weight } = DETAIL_DATA
    useEffect(() => {
        DETAIL_DATA?.key === '' && redirect('/')
    }, [DETAIL_DATA])
    return (
        <div className="bg-slate-200 flex-col md:space-x-5  flex  items-center  justify-between w-[100vw] sm:px-6 md:px-10 lg:max-w-5xl border rounded-md p-4 md:p-8">
            <Header title={key} />
            <div className="flex flex-col w-full items-center">
                <Image src={backSprite} alt={key} width={200} height={200} />
                <div className="flex flex-col w-full space-y-2">
                    <div className="grid grid-cols-1 sm:grid-cols-3 md:grid-col-4 w-full gap-2 ">
                        <div className=" flex justify-center flex-col items-center border w-full bg-slate-300 font-bold">
                            <p className="text-lg">Color: </p>
                            <p className="text-2xl">{color || '-'}</p>
                        </div>
                        <div className=" flex justify-center flex-col items-center border w-full bg-slate-300 font-bold">
                            <p className="text-lg">Height: </p>
                            <p className="text-2xl">{height || '-'}</p>
                        </div>
                        <div className=" flex justify-center flex-col items-center border w-full bg-slate-300 font-bold">
                            <p className="text-lg">Weight: </p>
                            <p className="text-2xl">{weight || '-'}</p>
                        </div>

                    </div>
                    <div className="w-full grid gap-2">
                        <div className=" flex justify-center flex-col items-center border w-full bg-slate-300 font-bold">
                            <p className="text-lg">Species: </p>
                            <p className="text-2xl">{species || '-'}</p>
                        </div>
                        <div className=" flex justify-center flex-col items-center border w-full bg-slate-300 font-bold">
                            <p className="text-lg">Types: </p>
                            <p className="text-2xl">{types.map((data, key) => <p key={key}>{data.name}</p>) || '-'}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}
export default page