import gql from 'graphql-tag';

export const GET_ALL_POKEMON = gql`
{
    getAllPokemon(take: 20) {
      types {
        name
      }
      color
      key
      weight
      height
      species
      backSprite
    }
  }
  
`;
