import type { Query } from '@favware/graphql-pokemon';
export interface GraphQLPokemonResponse<K extends keyof Omit<Query, '__typename'>> {
  datas: Record<K, Omit<Query[K], '__typename'>>;
}

export type typePokemon = {
  name: string
}
export interface IPokemonDetail {
  color: string,
  height: number,
  backSprite:string,
  key: string,
  species: string,
  types:typePokemon[]
  weight: number
}
export interface IPokemonAPI {
  getAllPokemon : IPokemonDetail[]
}
