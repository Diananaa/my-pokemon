import { IPokemonDetail } from '@/gqls/pokemon.types';
import { atom } from 'jotai';

export const DATA_DETAIL_POKEMON = atom<IPokemonDetail>({
    color: '',
    height: 0,
    key: '',
    backSprite:'',
    species: '',
    types:[],
    weight: 0
})

