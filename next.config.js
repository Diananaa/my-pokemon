/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: ['play.pokemonshowdown.com'],
      },
      typescript: {
        ignoreBuildErrors: true
      }
}

module.exports = nextConfig
